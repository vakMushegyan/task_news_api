import React from 'react';
import Footer from "./components/Footer";
import Nav from "./components/nav/Nav";
import Home from './components/Home';
import {Route} from 'react-router-dom';
import SourcesContainer from "./components/source/SourcesContainer";
import SearchResults from "./components/SearchResults";


class App extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Nav/>
                <main>
                    <Route exact path='/' component={Home} />
                    <Route exact path='/sources' component={SourcesContainer} />
                    <Route exact path='/search' component={SearchResults} />
                </main>
                <Footer/>
            </React.Fragment>
        )
    }
}

export default App;
