import React from 'react';
import Axios from "axios";
import Source from "./source/Sources";

class Home extends React.Component{
    state = {
        latestNews: []
    };

    componentDidMount() {
        Axios.get('https://newsapi.org/v2/top-headlines?country=us&apiKey=92a783b26a04461a89031f9cef08c157')
            .then( (latestNewsData) => {
                this.setState({
                    latestNews: latestNewsData.data.articles
                })
            } )
    }

    render() {
        return (
           <section>
               <Source sources={this.state.latestNews} />
           </section>
        )
    }

}

export default Home;
