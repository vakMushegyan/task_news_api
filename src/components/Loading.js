import React from 'react';


class Loading extends React.Component{
    state = {
        content: this.props.text
    };

    componentDidMount() {
        const {text, speed} = this.props;
        this.interval = window.setInterval( () => {
            this.state.content === text + '...' ?
                this.setState( {content: text} ) :
                this.setState(({content}) => ({content: content + '.'}) )
        }, speed )
    }

    componentWillUnmount() {
        window.clearInterval(this.interval);
    }

    render() {
        return (
            <div>
                {this.state.content}
            </div>
        )
    }
}

export default Loading;
