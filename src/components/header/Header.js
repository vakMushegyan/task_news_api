import React from 'react';
import PropTypes from 'prop-types';


class Header extends React.Component {
    static propTypes = {
        headingLevel: PropTypes.string,
        headingText: PropTypes.string
    };
    static defaultProps = {
        headingLevel: '1',
        headingText: 'Heading'
    };
    render() {
        const H = `h${this.props.headingLevel}`;
        return (
            <header>
                <H>{this.props.headingText}</H>
            </header>
        )
    }
}

export default Header;
