import React from 'react';
import {withRouter} from 'react-router-dom';
import Axios from "axios";
import queryString from 'query-string';
import Source from "./source/Sources";

class SearchResults extends React.Component{
    state = {
        searchResults: []
    };
    componentDidMount() {
        this.handleSubmit(this.props.location.search);
        this.unlisten = this.props.history.listen( ({pathname, search}) => {
            console.log('listening')
            if (pathname === '/search'){
                this.handleSubmit(search);
            }
        } )
    }

    componentWillUnmount() {
        this.unlisten();
    }

    handleSubmit = (s) => {
        let {search} = queryString.parse(s);
        if (search) {
            search = search.trim();
            search = search.replace(/\s+/g, ',');
        }
        const searchValue = search ? ` https://newsapi.org/v2/everything?q=${search}&apiKey=92a783b26a04461a89031f9cef08c157`
            : `https://newsapi.org/v2/top-headlines?country=us&apiKey=92a783b26a04461a89031f9cef08c157`;
        Axios.get(searchValue).then( (searchResults) => {
            console.log(searchResults, 'SEARCH  RESULTS');
            this.setState({
                searchResults: searchResults.data.articles
            })
        } );
    };

    render() {
        return (
            <div>
                <Source sources={this.state.searchResults} />
            </div>
        )
    }

}

export default withRouter(SearchResults);


// https://newsapi.org/v2/everything?q=bitcoin&apiKey=92a783b26a04461a89031f9cef08c157
