import React from 'react';

class ContactUs extends React.Component {
    state = {
        name: '',
        email: '',
        message: '',
    };
    render() {
        return (
            <form>
                <label htmlFor="name">
                    Name:
                </label>
                <input
                    id='name'
                    name='name'
                    onChange={this.handleChange}
                    value={this.state.name}
                    type="text"/>
                <label htmlFor="email">
                    Email:
                </label>
                <input
                    id='email'
                    onChange={this.handleChange}
                    value={this.state.email}
                    name='email'
                    type="text"/>
                <label htmlFor="message">
                    Message:
                </label>
                <textarea
                    id='message'
                    rows={5}
                    name='message'
                    value={this.state.message}
                    onChange={this.handleChange}
                />
            </form>
        )
    }
}

export default ContactUs;
