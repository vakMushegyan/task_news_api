import React from 'react';
import {withRouter} from 'react-router-dom';
import queryString from 'query-string';
import Axios from "axios";
import Source from "./Sources";
import Loading from "../Loading";


class SourcesContainer extends React.Component {

    state = {
        chosenTopic: '',
        filteredSources: {

        }
    };


    getLoading = () => {
        return !!this.state.filteredSources[this.state.chosenTopic]
    };

    componentDidMount() {
        const {id} = queryString.parse(this.props.location.search);
        Axios.get(`https://newsapi.org/v2/everything?sources=${id}&apiKey=92a783b26a04461a89031f9cef08c157`)
            .then((sourceData) => {
                this.setState({
                    filteredSources: {
                        ...this.state.filteredSources,
                        [id]: sourceData.data.articles
                    },
                    chosenTopic: id
                })
            });
        this.unlisten = this.props.history.listen(({pathname, search}) => {
            if (pathname === '/sources') {
                const {id} = queryString.parse(search);
                this.setState({
                    chosenTopic: id
                });
                if (!this.state.filteredSources[id]) {
                    Axios.get(`https://newsapi.org/v2/everything?sources=${id}&apiKey=92a783b26a04461a89031f9cef08c157`)
                        .then((sourceData) => {
                            this.setState({
                                filteredSources: {
                                    ...this.state.filteredSources,
                                    [id]: sourceData.data.articles
                                },
                                chosenTopic: id
                            }, () => {
                                this.forceUpdate();
                            });
                        });
                } else {
                    this.forceUpdate()
                }

            }
        })
    }

    componentWillUnmount() {
        this.unlisten();
    }


    render() {
        return (
            <div>
                {
                    this.getLoading() ?
                        <Source sources={this.state.filteredSources[this.state.chosenTopic]}/> :
                        <Loading text={'loading'} speed={200} />
                }

            </div>
        )
    }
}

export default withRouter(SourcesContainer);
