import React from 'react';

class Source extends React.Component {
    render() {
        const {sources} = this.props;
        return (
            <div>
                <ul>
                    {
                        sources.map(item => {
                            return (
                                <li key={item.url}>
                                    {item.content}
                                </li>
                            )
                        })
                    }
                </ul>
            </div>
        )
    }
}

export default Source;
