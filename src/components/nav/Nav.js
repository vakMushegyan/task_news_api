import React from 'react';
import {NavLink} from "react-router-dom";
import Axios from "axios";
import Search from "./Search";
import ContactUs from "../ContactUs";


class Nav extends React.Component {
    constructor() {
        super();
        this.state = {
            sources: [],
            showModal: false
        }
    }

    componentDidMount() {
        Axios.get('https://newsapi.org/v2/sources?language=en&apiKey=92a783b26a04461a89031f9cef08c157')
            .then((sourcesData) => {
                const data = sourcesData.data.sources.filter( (item, index) => {
                    return index < 3
                } );
                this.setState({
                    sources: data
                })
            });
    }

    handleContactUs = () => {
        this.setState({
            showModal: !this.state.showModal
        })
    };

    render() {
        const linkArr = this.state.sources.map( (item) => {
            return (
                <div key={item.id} >


                <NavLink
                    exact
                    to={{
                    pathname: '/sources',
                    search: `?id=${item.id}`
                }} >
                    {item.name}
                </NavLink>
                </div>

            )
        } );
        return (
            <nav>
                <NavLink to='/'>Home</NavLink>
                {linkArr}
                <Search/>
                <button onClick={this.handleContactUs} >
                    Contact Us
                </button>

                {this.state.showModal && <ContactUs/>}
            </nav>
        )
    }
}



export default Nav;
