import React from 'react';
import {withRouter} from 'react-router-dom';

class Search extends React.Component{
    state = {
        searchValue: ''
    };

    componentDidMount() {
        console.log(this.props.history)
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.history.push({pathname: '/search', search: `?search=${this.state.searchValue}`});
    };
    handleChange = (e) => {
        this.setState({
            searchValue: e.target.value
        })

    };
    render() {
        return (
            <form onSubmit={this.handleSubmit} >
                <input
                    onChange={this.handleChange}
                    value={this.state.searchValue}
                    type="text"/>
            </form>
        )
    }

}

export default withRouter(Search);
